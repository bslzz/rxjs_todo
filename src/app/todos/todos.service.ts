import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { FilterEnum } from '../types/filter.enum';
import { ITodos } from '../types/todos.interface';

@Injectable({
  providedIn: 'root',
})
export class TodosService {
  public todos$ = new BehaviorSubject<ITodos[]>([]);
  public filterTodos$ = new BehaviorSubject<FilterEnum>(FilterEnum.all);

  constructor() {}

  public addTodo(text: string): void {
    const todos: ITodos = {
      id: Math.random().toString(16),
      text,
      isCompleted: false,
    };
    const updatedTodos = [...this.todos$.getValue(), todos];
    this.todos$.next(updatedTodos);
  }

  public toggleAllTodos(isChecked: boolean): void {
    const updatedTodos = this.todos$.getValue().map((todo) => ({
      ...todo,
      isCompleted: isChecked,
    }));
    this.todos$.next(updatedTodos);
  }

  public changeFilter(filter: FilterEnum): void {
    this.filterTodos$.next(filter);
  }

  public updateTodo(id: string, text: string): void {
    const updatedTodos = this.todos$
      .getValue()
      .map((todo) => (todo.id === id ? { ...todo, text } : todo));
    this.todos$.next(updatedTodos);
  }

  public removeTodo(id: string): void {
    const updatedTodos = this.todos$
      .getValue()
      .filter((todo) => todo.id !== id);
    this.todos$.next(updatedTodos);
  }

  public toggleTodo(id: string): void {
    const updatedTodos = this.todos$
      .getValue()
      .map((todo) =>
        todo.id === id ? { ...todo, isCompleted: !todo.isCompleted } : todo
      );
    this.todos$.next(updatedTodos);
  }
}
