import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { map } from 'rxjs';
import { TodosService } from 'src/app/todos/todos.service';
import { ITodos } from 'src/app/types/todos.interface';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
})
export class TodoComponent implements OnInit, OnChanges {
  @Input('todo') todo!: ITodos;
  @Input('isEditing') isEditingMode!: boolean;
  @Output('setEditingId') setEditingIdEvent: EventEmitter<string | null> =
    new EventEmitter<string | null>();

  @ViewChild('editTextInput') editTextInput!: ElementRef;

  public editingText: string = '';

  constructor(private todoService: TodosService) {}

  ngOnInit(): void {
    this.editingText = this.todo.text;
  }

  // ngchanges works only with input properties
  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnChanges', changes['isEditingMode']?.currentValue);
    if (changes['isEditingMode']?.currentValue) {
      // due to ngif in html, we need to set timeout to wait for the view to be rendered
      setTimeout(() => {
        this.editTextInput?.nativeElement.focus();
      }, 0);
    }
  }

  public setTodoInEditMode(id: string) {
    console.log('setTodoInEditMode', id);
    this.setEditingIdEvent.emit(id);
  }

  public removeTodo(id: string) {
    this.todoService.removeTodo(id);
  }

  public toggleTodo(): void {
    this.todoService.toggleTodo(this.todo.id);
  }

  public changeText(event: Event): void {
    const target: HTMLInputElement = event.target as HTMLInputElement;
    this.editingText = target.value;
  }

  public updateTodo(id: string): void {
    this.todoService.updateTodo(id, this.editingText);
    this.setEditingIdEvent.emit(null);
    this.editingText = '';
  }
}
