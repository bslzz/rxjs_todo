import { Component, OnInit } from '@angular/core';
import { TodosService } from 'src/app/todos/todos.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  public text: string = '';
  constructor(private todosService: TodosService) {}

  ngOnInit(): void {}

  public changeText(event: Event): void {
    const target: HTMLInputElement = event.target as HTMLInputElement;
    this.text = target.value;
  }

  public addTodo(): void {
    this.todosService.addTodo(this.text);
    this.text = '';
  }
}
