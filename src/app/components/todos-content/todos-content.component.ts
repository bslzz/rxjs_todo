import { Component, OnInit } from '@angular/core';
import { combineLatest, map, Observable } from 'rxjs';
import { TodosService } from 'src/app/todos/todos.service';
import { FilterEnum } from 'src/app/types/filter.enum';
import { ITodos } from 'src/app/types/todos.interface';

@Component({
  selector: 'app-todos-content',
  templateUrl: './todos-content.component.html',
  styleUrls: ['./todos-content.component.scss'],
})
export class TodosContentComponent implements OnInit {
  public visibleTodos$!: Observable<ITodos[]>;
  public noTodos$!: Observable<boolean>;
  public isAllTodosSelected$!: Observable<boolean>;
  public isEditingMode: string | null = null;

  constructor(private todosService: TodosService) {}

  ngOnInit(): void {
    this.getAllTodos();
    this.todoListsExist();
    this.selectAllTodos();
  }

  public selectAllTodos(): void {
    this.isAllTodosSelected$ = this.todosService.todos$.pipe(
      map((todos) => todos.every((todo) => todo.isCompleted))
    );
  }

  public todoListsExist(): void {
    this.noTodos$ = this.todosService.todos$.pipe(
      map((todos) => !todos.length)
    );
  }

  public getAllTodos(): void {
    this.visibleTodos$ = combineLatest([
      this.todosService.todos$,
      this.todosService.filterTodos$,
    ]).pipe(
      map(([todos, filter]: [ITodos[], FilterEnum]) => {
        const types = {
          [FilterEnum.active]: () => todos.filter((todo) => !todo.isCompleted),
          [FilterEnum.completed]: () =>
            todos.filter((todo) => todo.isCompleted),
          [FilterEnum.all]: () => todos,
        };
        return types[filter]() ?? [];
        // if (filter === FilterEnum.active) {
        //   return todos.filter((todo) => !todo.isCompleted);
        // } else if (filter === FilterEnum.completed) {
        //   return todos.filter((todo) => todo.isCompleted);
        // }
        // return todos;
      })
    );
  }

  public toggleAllTodos(event: Event): void {
    const target: HTMLInputElement = event.target as HTMLInputElement;
    this.todosService.toggleAllTodos(target.checked);
  }

  public setEditingId(id: string | null): void {
    this.isEditingMode = id;
  }
}
