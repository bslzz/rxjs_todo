import { Component, OnInit } from '@angular/core';
import { map, Observable } from 'rxjs';
import { TodosService } from 'src/app/todos/todos.service';
import { FilterEnum } from 'src/app/types/filter.enum';
import { ITodos } from 'src/app/types/todos.interface';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit {
  public noTodos$!: Observable<boolean>;
  public activeTodosCount$!: Observable<number>;
  public itemsLeftText$!: Observable<string>;
  public filterEnum = FilterEnum;
  public filter$!: Observable<FilterEnum>;

  constructor(private todosService: TodosService) {}

  ngOnInit(): void {
    this.todoListsExist();
    this.activeTodos();
    this.itemsLeftText();
    this.filter$ = this.todosService.filterTodos$;
  }

  public todoListsExist(): void {
    this.noTodos$ = this.todosService.todos$.pipe(
      map((todos) => !todos.length)
    );
  }

  public activeTodos(): void {
    this.activeTodosCount$ = this.todosService.todos$.pipe(
      map((todos) => todos.filter((todo) => !todo.isCompleted).length)
    );
  }

  public itemsLeftText(): void {
    this.itemsLeftText$ = this.activeTodosCount$.pipe(
      map((todos) => {
        if (todos === 1) {
          return 'item left';
        } else {
          return 'items left';
        }
      })
    );
  }

  public changeFilter(event: Event, filterEnum: FilterEnum): void {
    event.preventDefault();
    this, this.todosService.changeFilter(filterEnum);
  }
}
